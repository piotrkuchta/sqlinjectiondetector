from communication import (task_queue, result_queue, Task, Result)
from page_explorer import PageExplorer
from log import log


class CrawlWorker(object):

    def __init__(self, task_queue=task_queue, result_queue=result_queue):
        self.task_queue = task_queue
        self.result_queue = result_queue
        self.page_explorer = PageExplorer()

    def execute(self):
        """
        Take a task from the task_queue, pass it to the page_explorer to process
        and put result on result_queue.
        """
        task = self.task_queue.get()
        log.info('Fetching ' + task.url.url_string)
        result = self.page_explorer.processTask(task)
        log.info('Discovered {} new link(s) on {}'.format(len(result.urls), result.url.url_string))
        self.result_queue.put(result)
