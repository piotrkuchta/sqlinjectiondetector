# SQLInjectionDetector

Web crawler and SQL injection detector. Diagram of the architecture: 

https://www.lucidchart.com/invitations/accept/9ef60020-81be-4c20-ad0b-c459dafa9735

## Components of the system

### Crawler

Crawler consists of following components:

* Single **crawler process** sending task messages (URL to fetch) to workers and receiving results from them.
* **task_queue** is a message queue used by the crawler process to send tasks to workers. After the task is put on the queue any of the idle workers can pick it up and process it.
* **result_queue** is a message queue used by the workers to send results of processing tasks (URLs found on the page they fetched) back to the crawler.
* **Register**, a database of URLs being processed. Only the Crawler uses the register to keep track of discovered and fetched URLs and relationship (linking) between them.

Note, the architecture described here is scalable as it allows for many CrawlWorker processes to be distributed on many machines.
However, the prototype implementation is single threaded for simplicity and ease of development.

### CrawlWorker

There may be many distributed workers deployed in the running system. The crawler does not need to know their exact location or number. The communication between the crawler and the workers is handled by the message queue system. Worker's job is to:

* pick up a message with a task from the **task_queue**; the task contains a URL
* retrieve a page located by the URL
* use **PageExplorer** to find SQL injection vulnerabilities and parse the page to find URLs to other, linked, pages in the domain
* send back the URLs found back to the crawler via the **result_queue**

#### Error handling
In case of a failure to retrieve a page due to e.g. a network outage, the worker resubmits the task back to the **task_queue**. The task message has a field indicating the number of retries and it will only be resubmitted if the number is below a limit.


## Crawler details
The crawler essentially performs a search of the graph defined by pages (nodes) and links (edges) of the website.

It uses the workers to do the hard work of retrieving web pages, detecting vulnerabilities, and parsing them to discover new links. 

Crawler uses the register (database of URLs) to represent website's graph structure and keep track of the pages that have been already retrieved and those that stil need to be retrieved.

### Crawling algorithm
After sending the first task to retrieve the top level URL (seed) to workers, the crawler waits for new messages arriving in the **result_queue** and for each performs what follows:

* Decode the message retrieved from the **result_queue**. It contains a UUID of a page that a worker managed to retrieve and a number of URLs found on that page.
* Find out which of the URLs found on the page are new (not previously seen) by looking them up in the **register**.
* Mark the page as successfully retrieved in the key-value store of the **register**. This is encoded by associating the number of new links discovered in the page with the page URL's UUID. This number will be initially zero or more and will decrease as the pages pointed to by the new URLs found on the page are retrieved.
* Register the new URLs by saving them in the database and associating their UUID with -1 (meaning: number of new links in them is not yet known) in the key-value store.
* Send a message with a task to retrieve each newly discovered URL to workers via the **task_queue**
* Lookup the parent of the retrieved page in the **register** and decrease the number associated with its UUID.
* Check if the number associated with top level URL is zero. If yes, then it means the whole website has been successfully crawled and the crawler can terminate.

### Register (database of URLs)

The register is a database used by the crawler to store:

* the URLs found on the website
* connections between pages: every URL has a pointer to its parent in the search tree
* information if the URL and its children have been retrieved

The database has to be persistent in order to allow for resuming work after system and hardware failures.

#### URL data

Information about each URL contains:

* **uuid**: unique identifier of a URL; 128 bits or 36 characters in a standard text representation, e.g. `95d21b90-965d-3676-a1db-f0e744d27484`
* **url_string**: the Uniform Location Locator or "web address", e.g. `http://www.example.com/index.html`
* **method**: `GET` or `POST`
* **payload**: dictionary with form data if the method is `POST`
* **referrer**: UUID of the parent URL

#### Structure of the database

Assuming the website does not change during crawling, 
an URL data stored in the database does not need to be updated once it is saved. 
Therefore, storing a serialized representation of URL objects of the implementation language, 
as a blob or string, should be sufficient.

The URL database stores two types of pairs:

    <UUID, serialized URL>
    
 and
 
    <UUUID, number of child URLs to be retrieved>

The former, as mentioned above, is a static data, the values of the later need to be updated. 
A key-value store will be suitable for this data.

Note, to allow recovery the database should be persistent. 
However, the prototype implementation uses a basic Python dictionary.


### Communication between the crawler and workers

#### Message Queues
The message queue system is a backbone of the crawler's architecture. It should provide:

* **Scalability**, it should allow for scalling from 1 to 5000 workers.
* **Reliability**, in case of a failure of one or more workers or the crawler it should be possible to resume the crawling. Therefore, the message queing system used should persist the messages on both the **task_queue** and the **result_queue**.

Note, the prototype implementation uses Python's queue.Queue, which is not persistent.

#### Messages
The system uses two types of messages passed between the crawler and workers: **Task** and **Result**.

##### Task

* **url**: URL of a page to be fetched, a record as described above
* **depth**: depth of recursion, a small positive integer
* **retries**: number of attempted fetches

##### Result

* **urls**: list of the URLs found on page
* **url**:  URL of the page that was fetched and parsed or just its UUID
* **depth**: depth of recursion (not strictly necessary, as it can be saved and looked up in the register)


## Testing

pytest has been used as a unit testing framework. The tests rely on DVWA running and being accessible.
Ideally, a mocking library like httmock or flask's testing functionality should be used to make the unit tests lightweight,
fast and standalone / not dependant on a virtual machine.
However, given the time constraints it was practical to make the tests use a live DVWA.

### Configuration

Before running the tests save IP of running DVWA in a file dvwa.cfg in the source code's directory.