from communication import (URL, Task, Result, task_queue, result_queue)
from register import Register
from crawl_worker import CrawlWorker
from abc import ABC, abstractmethod


class Crawler(ABC):
    def __init__(self, task_queue=task_queue, result_queue=result_queue, register=None):
        self.task_queue = task_queue
        self.result_queue = result_queue
        self.register = register or Register()
        super().__init__()

    @abstractmethod
    def crawl(self, url_string):
        pass


class SingleThreadedCrawler(Crawler):
    """
    Single threaded implementation of Crawler. Creates one CrawlWorker and works with it.
    """

    def __init__(self, task_queue=task_queue, result_queue=result_queue, register=None):
        super().__init__(task_queue, result_queue, register)
        self.worker = CrawlWorker(task_queue=self.task_queue, result_queue=self.result_queue)

    def crawl(self, url_string: str):
        top_url = URL.create(url_string)
        task = Task(top_url, 0, 0)
        self.register.setTop(top_url)
        self.task_queue.put(task)
        while not self.task_queue.empty():
            self.worker.execute()
            result = self.result_queue.get()
            discovered = [url for url in result.urls if not self.register.known(url)]
            self.register.markFetched(result.url, discovered)
            for url in discovered:
                self.task_queue.put(Task(url, result.depth + 1, 0))

            if self.register.allFetched():
                # ALL DONE!
                break

