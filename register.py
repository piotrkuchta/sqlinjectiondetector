from communication import URL
from typing import List


def url_key_s(url_string: str) -> str: return 'url:{}'.format(url_string)


def n_key_s(url_string: str) -> str: return 'n:{}'.format(url_string)


def url_key(url: URL) -> str: return url_key_s(url.uuid)


def n_key(url: URL) -> str: return n_key_s(url.uuid)


class Register(object):
    def __init__(self):
        self.db = {}  # could be, for example, redis.Redis()

    def setTop(self, top: URL):
        """Remember the starting point of the crawl."""
        self.db['top_url_uuid'] = top.uuid
        self.add(top)

    def top(self):
        top_uuid = self.db['top_url_uuid']
        return self.db[url_key_s(top_uuid)]

    def add(self, url: URL):
        """Add url to register. Mark number of links on the page as unknown (-1)."""
        self.db[n_key(url)] = -1
        self.db[url_key(url)] = url

    def markFetched(self, url: URL, discovered: List[URL]):
        """
        Mark url as retrieved and processed.
        Save number of new links discovered as number of links to be fetched before the url is completely done.
        If there is a referrer of the URL then decrease number of links to be fetched.
        Add new links to the register (now they are known).
        """
        self.db[n_key(url)] = len(discovered)
        if url.referrer is not None:
            # key of the value storing number of links found on referrer still to be fetched
            key = n_key_s(url.referrer)
            n = int(self.db[key])
            self.db[key] = n - 1
        for u in discovered:
            self.add(u)

    def known(self, url: URL):
        return n_key(url) in self.db

    def allFetched(self):
        """
        Check if all links found on top URL already fetched.
        """
        # Retrieve number of links found on top URL that still has not been been fetched
        n = int(self.db[n_key(self.top())])
        # If it is zero then we have crawled everything
        return n == 0

