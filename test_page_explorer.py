from page_explorer import PageExplorer
from communication import (URL, Task)
from configuration import DVWA_URL


def test_page_explorer_login():
    pe = PageExplorer()
    result = pe.processTask(Task(URL.create(DVWA_URL), 0, 0))
    assert len(result.urls) == 17


if __name__ == '__main__':
    import pytest
    pytest.main() #args=['-s'])
