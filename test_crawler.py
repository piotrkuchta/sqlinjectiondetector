from configuration import DVWA_URL
from crawler import SingleThreadedCrawler
from register import Register
import queue


def test_crawler():
    task_queue = queue.Queue()
    result_queue = queue.Queue()
    register = Register()
    crawler = SingleThreadedCrawler(task_queue, result_queue, register)
    crawler.crawl(DVWA_URL)
    assert register.allFetched()
