import logging as log

log.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
log.getLogger().setLevel(log.INFO)

