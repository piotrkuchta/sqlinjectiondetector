import queue
import uuid
from typing import NamedTuple, List

task_queue = queue.Queue()    # type: queue.Queue
result_queue = queue.Queue()  # type: queue.Queue


class URL(NamedTuple):

    @classmethod
    def create(cls, url, method='GET', payload=None, referrer=None):
        return cls(str(uuid.uuid3(uuid.NAMESPACE_URL, url)), url, method, payload, referrer)

    uuid: str
    url_string: str
    method: str
    payload: dict
    referrer: str    # uuid of the referrer URL


class Task(NamedTuple):
    url: URL         # URL of a page to be fetched
    depth: int       # depth of recursion
    retries: int     # number of attempted fetches


class Result(NamedTuple):
    urls: List[URL]  # list of the URLs found on page
    url: URL         # URL of the page that was fetched abd parsed
    depth: int       # depth of recursion

