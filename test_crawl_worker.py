import queue
from crawl_worker import CrawlWorker
from configuration import DVWA_URL
from communication import (URL, Task, Result)


def test_crawl_worker():
    task_queue = queue.Queue()
    result_queue = queue.Queue()
    worker = CrawlWorker(task_queue=task_queue, result_queue=result_queue)

    task = Task(URL.create(DVWA_URL), 0, 0)
    task_queue.put(task)
    worker.execute()
    result = result_queue.get()
    assert isinstance(result, Result)
    assert result.url.url_string == DVWA_URL
    assert len(result.urls) > 0

