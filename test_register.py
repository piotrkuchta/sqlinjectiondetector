import queue
from crawl_worker import CrawlWorker
from register import Register
from configuration import DVWA_URL
from communication import (URL, Task, Result)


def test_register():
    task_queue = queue.Queue()
    result_queue = queue.Queue()
    worker = CrawlWorker(task_queue=task_queue, result_queue=result_queue)
    register = Register()

    top_url = URL.create(DVWA_URL)
    register.setTop(top_url)

    task = Task(top_url, 0, 0)
    task_queue.put(task)
    worker.execute()
    result = result_queue.get()

    new_links = [u for u in result.urls if not register.known(u)]
    register.markFetched(top_url, new_links)
    # pretend all links found on top_url traversed already
    for u in new_links:
        register.markFetched(u, [])

    assert register.allFetched()
